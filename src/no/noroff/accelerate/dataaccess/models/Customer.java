package no.noroff.accelerate.dataaccess.models;

public record Customer(String Id,
                       String CompanyName,
                       String ContactName,
                       String City) {
}
