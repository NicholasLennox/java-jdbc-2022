package no.noroff.accelerate.dataaccess;

import no.noroff.accelerate.dataaccess.models.Customer;
import no.noroff.accelerate.dataaccess.util.SqlLiteConnectionHelper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAO {
    public List<Customer> getAllCustomers(){
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT Id, CompanyName, ContactName, City FROM Customer";
        try(Connection conn =
                    DriverManager.getConnection(
                            SqlLiteConnectionHelper.getConnectionString())) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Use the results
            while(result.next()){
                Customer cust = new Customer(
                        result.getString("Id"),
                        result.getString("CompanyName"),
                        result.getString("ContactName"),
                        result.getString("City")
                );
                customers.add(cust);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }

    public List<Customer> getCustomerById(String id){
        List<Customer> customers = new ArrayList<>();
        String sql = "SELECT Id, CompanyName, ContactName, City FROM Customer" +
                " WHERE Id = ?";
        try(Connection conn =
                    DriverManager.getConnection(
                            SqlLiteConnectionHelper.getConnectionString())) {
            // Write statement
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,id);
            // Execute statement
            ResultSet result = statement.executeQuery();
            // Use the results
            while(result.next()){
                Customer cust = new Customer(
                        result.getString("Id"),
                        result.getString("CompanyName"),
                        result.getString("ContactName"),
                        result.getString("City")
                );
                customers.add(cust);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return customers;
    }
}
