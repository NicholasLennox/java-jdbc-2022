package no.noroff.accelerate;

import no.noroff.accelerate.dataaccess.CustomerDAO;

public class Main {

    public static void main(String[] args) {
        CustomerDAO custDAO = new CustomerDAO();
        custDAO.getCustomerById("ALFKI").stream().forEach(c -> System.out.println(c.toString()));
    }
}
